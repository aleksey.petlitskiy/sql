CREATE TABLE  employees(
	id serial primary key,
	employee_name varchar(50) unique not null
);

insert into employees(employee_name)
values('Anna B'),
	  ('Inna'),
	  ('Valerii B'),
	  ('Kirill B'),
	  ('Oleksii B'),
	  ('Oleg B'),
	  ('Bohdan N'),
	  ('Valia B'),
	  ('Valera B'),
	  ('Valeriia B'),
	  ('Oleksandra B'),
	  ('Viktor B'),
	  ('Hliob B'),
	  ('Anatolii B'),
	  ('Polina B'),
	  ('Kristina B'),
	  ('Illa B'),
	  ('Rita B'),
	  ('Vkhtang B'),
	  ('Irina B'),
	  ('Serhii B'),
	  ('Andrii B'),
	  ('Ivan B'),
	  ('Pavlo Ch'),
	  ('Svetlana B'),
	  ('Devid B'),
	  ('Chris B'),
	  ('Daren B'),
	  ('Darius B'),
	  ('Phill B'),
	  ('Morriss B'),
	  ('Jack B'),
	  ('Andrew B'),
	  ('Bill B'),
	  ('John B'),
	  ('Eshton B'),
	  ('Lisa B'),
	  ('Mira B'),
	  ('Morghan B'),
	  ('Tom B'),
	  ('Tim B'),
	  ('Elena B'),
	  ('Sonia B'),
	  ('Simon B'),
	  ('Sara B'),
	  ('Ron B'),
	  ('Umi B'),
	  ('George B'),
	  ('Fill B'),
	  ('Dolores B'),
	  ('William B'),
	  ('Euhen B'),
	  ('Maxim B'),
	  ('Ihor B'),
	  ('Inna B'),
	  ('Nina B'),
	  ('Paul B'),
	  ('Robert B'),
	  ('Chriss B'),
	  ('Chloe B'),
	  ('Nill B'),
	  ('Hylary B'),
	  ('Annabel B'),
	  ('Anastasii B'),
	  ('Julia B'),
	  ('Alla B');
	  
CREATE TABLE  salary_new(
	id serial primary key,
	monthly_salary int not null
);

insert into salary_new(monthly_salary)
values('1000'),
	  ('1100'),
	  ('1200'),
	  ('1300'),
	  ('1400'),
	  ('1500'),
	  ('1600'),
	  ('1700'),
	  ('1800'),
	  ('1900'),
	  ('2000'),
	  ('2100'),
	  ('2200'),
	  ('2300'),
	  ('2400'),
	  ('2500');
	  
CREATE TABLE  employee_salary_new(
	id serial primary key,
	employee_id int unique not null,
	salary_new_id int not null
);

insert into employee_salary_new(employee_id, salary_new_id)
values(56,1),
	  (57,2),
	  (58,3),
	  (59,4),
	  (60,5),
	  (61,6),
	  (62,7),
	  (63,8),
	  (64,9),
	  (65,10),
	  (66,11),
	  (67,12),
	  (68,12),
	  (69,13),
	  (70,14),
	  (71,1),
	  (72,2),
	  (73,3),
	  (74,4),
	  (75,5),
	  (76,6),
	  (77,7),
	  (78,8),
	  (79,8),
	  (80,9),
	  (81,1),
	  (82,2),
	  (83,3),
	  (84,4),
	  (85,6),
	  (188,8),
	  (167,1),
	  (189,2),
	  (199,3),
	  (200,4),
	  (192,5),
	  (176,7),
	  (163,8),
	  (138,9);
	  
CREATE TABLE it_roles(
	id serial primary key,
	role_name int unique not null 
);

alter table it_roles
	drop column role_name;
	
alter table it_roles
	add role_name varchar(30);
	
insert into it_roles(role_name)
values ('Junior Python developer'),
	   ('Middle Python developer'),
	   ('Senior Python developer'),
	   ('Junior Java developer'),
	   ('Middle Java developer'),
	   ('Senior Java developer'),
	   ('Junior JavaScript developer'),
	   ('Middle JavaScript developer'),
	   ('Senior JavaScript developer'),
	   ('Junior Manual QA engineer'),
	   ('Middle Manual QA engineer'),
	   ('Senior Manual QA engineer'),
	   ('Project Manager'),
	   ('Designer'),
	   ('HR'),
	   ('CEO'),
	   ('Sales manager'),
	   ('Junior Automation QA engineer'),
	   ('Middle Automation QA engineer'),
	   ('Senior Automation QA engineer');
	   
select * from employees;
select * from it_roles;
select * from roles_employees;


CREATE table roles_employees(
	id serial primary key,
	employee_id int unique not null,
	role_id int not null,
	foreign key (employee_id)
		references employees(id),
	foreign key (role_id)
		references it_roles(id)
);

drop table roles_employees;

insert into roles_employees(employee_id, role_id)
values (1,1),
 	   (2,2),
 	   (3,3),
 	   (4,4),
 	   (5,5),
 	   (6,6),
 	   (7,7),
 	   (8,8),
 	   (9,9),
 	   (10,10),
 	   (11,11),
 	   (12,12),
 	   (13,13),
 	   (14,14),
 	   (15,15),
 	   (16,16),
 	   (17,17),
 	   (18,18),
 	   (19,19),
 	   (20,20),
 	   (21,1),
 	   (22,2),
 	   (23,3),
 	   (24,4),
 	   (25,5),
 	   (26,6),
 	   (27,7),
 	   (28,8),
 	   (29,8),
 	   (30,9),
 	   (31,10),
 	   (32,11),
 	   (33,12),
 	   (34,12),
 	   (35,14),
 	   (36,1),
 	   (37,2),
 	   (38,3),
 	   (39,4),
 	   (40,5);
 	   