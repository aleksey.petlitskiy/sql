1. Вывести всех работников чьи зарплаты есть в базе, вместе с зарплатами.
select employee_name, monthly_salary from
employees inner join salary_new
on employees.id = salary_new_id;

2. Вывести всех работников у которых ЗП меньше 2000.
select employee_name, monthly_salary from
employees inner join salary_new
on employees.id = salary_new_id
where monthly_salary < 2000;

3. Вывести все зарплатные позиции, но работник по ним не назначен.
   (ЗП есть, но не понятно кто её получает.)
select employees.employee_name, salary_new.monthly_salary from
employees right join salary_new
on employees.id = salary_new_id
where employees.employee_name is null 

4. Вывести все зарплатные позиции меньше 2000, но работник по ним не назначен. (ЗП есть, но не понятно кто её получает.)
select employees.employee_name, salary_new.monthly_salary from
employees right join salary_new
on employees.id = salary_new_id
where employees.employee_name is null and salary_new.monthly_salary < 2000;

 5. Найти всех работников кому не начислена ЗП.
select employees.employee_name, salary_new.monthly_salary from
employees left join salary_new
on employees.id = salary_new_id
where salary_new.monthly_salary is null 

6. Вывести всех работников с названиями их должности.
select employees.employee_name, it_roles.role_name from
employees inner join it_roles
on employees.id = it_roles.id;

7. Вывести имена и должность только Java разработчиков.
select employees.employee_name, it_roles.role_name from
employees inner join it_roles
on employees.id = it_roles.id
where it_roles.role_name like '%Java dev%'

 8. Вывести имена и должность только Python разработчиков.
select employees.employee_name, it_roles.role_name from
employees inner join it_roles
on employees.id = it_roles.id
where it_roles.role_name like '%ython%'

9. Вывести имена и должность всех QA инженеров.
select employees.employee_name, it_roles.role_name from
employees inner join it_roles
on employees.id = it_roles.id
where it_roles.role_name like '%QA%'

 10. Вывести имена и должность ручных QA инженеров.
 select employees.employee_name, it_roles.role_name from
employees inner join it_roles
on employees.id = it_roles.id
where it_roles.role_name like '%Manual QA%'
 
 11. Вывести имена и должность автоматизаторов QA
 select employees.employee_name, it_roles.role_name from
employees inner join it_roles
on employees.id = it_roles.id
where it_roles.role_name like '%utomation QA%'

 12. Вывести имена и зарплаты Junior специалистов
select distinct employees.employee_name, salary_new.monthly_salary, it_roles.role_name from employees
inner join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where it_roles.role_name like '%unior%'

 13. Вывести имена и зарплаты Middle специалистов
select distinct employees.employee_name, salary_new.monthly_salary, it_roles.role_name from employees
inner join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where it_roles.role_name like '%iddle%'

 14. Вывести имена и зарплаты Senior специалистов
select distinct employees.employee_name, salary_new.monthly_salary, it_roles.role_name from employees
inner join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where it_roles.role_name like '%enior%'

 15. Вывести зарплаты Java разработчиков
select salary_new.monthly_salary from salary_new
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where it_roles.role_name like '%Java dev%'

 16. Вывести зарплаты Python разработчиков
select salary_new.monthly_salary from salary_new
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where it_roles.role_name like '%ython%'

 17. Вывести имена и зарплаты Junior Python разработчиков
select employees.employee_name, salary_new.monthly_salary from employees
inner join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where it_roles.role_name like 'Junior Py%'

 18. Вывести имена и зарплаты Middle JS разработчиков
select employees.employee_name, salary_new.monthly_salary from employees
inner join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where it_roles.role_name like 'Middle JavaS%'

 19. Вывести имена и зарплаты Senior Java разработчиков
select employees.employee_name, salary_new.monthly_salary from employees
inner join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where it_roles.role_name like 'Senior Java d%'

 20. Вывести зарплаты Junior QA инженеров
select salary_new.monthly_salary from employees
inner join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where it_roles.role_name like '%Junior Manual QA%'

 21. Вывести среднюю зарплату всех Junior специалистов
select avg(monthly_salary) from salary_new
inner join it_roles
on salary_new.salary_new_id = it_roles.id
where it_roles.role_name like 'Junior%'

 22. Вывести сумму зарплат JS разработчиков
select sum(monthly_salary) from it_roles
inner join salary_new
on salary_new.salary_new_id = it_roles.id
where it_roles.role_name like '%JavaScript%'

 23. Вывести минимальную ЗП QA инженеров
select min(monthly_salary) from it_roles
inner join salary_new
on salary_new.salary_new_id = it_roles.id
where it_roles.role_name like '%QA%'

 24. Вывести максимальную ЗП QA инженеров
select max(monthly_salary) from it_roles
inner join salary_new
on salary_new.salary_new_id = it_roles.id
where it_roles.role_name like '%QA%'

 25. Вывести количество QA инженеров
 select count(role_name) from it_roles
where it_roles.role_name like '%QA%'
 
 26. Вывести количество Middle специалистов.
select count(role_name) from it_roles
where it_roles.role_name like '%Middle%'

 27. Вывести количество разработчиков
select count(role_name) from it_roles
where it_roles.role_name like '%dev%'

 28. Вывести фонд (сумму) зарплаты разработчиков.
select sum(monthly_salary) from salary_new
inner join it_roles
on salary_new.id = salary_new.salary_new_id
where it_roles.role_name like '%dev%'

 29. Вывести имена, должности и ЗП всех специалистов по возрастанию
select employees.employee_name, it_roles.role_name, salary_new.monthly_salary from employees
join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
order by salary_new.monthly_salary asc 

 30. Вывести имена, должности и ЗП всех специалистов по возрастанию у специалистов у которых ЗП от 1700 до 2300
select employees.employee_name, it_roles.role_name, salary_new.monthly_salary from employees
join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where salary_new.monthly_salary between 1700 and 2300
order by salary_new.monthly_salary asc;
 
 31. Вывести имена, должности и ЗП всех специалистов по возрастанию у специалистов у которых ЗП меньше 2300
select employees.employee_name, it_roles.role_name, salary_new.monthly_salary from employees
join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where salary_new.monthly_salary < 1700;

 32. Вывести имена, должности и ЗП всех специалистов по возрастанию у специалистов у которых ЗП равна 1100, 1500, 2000
select employees.employee_name, it_roles.role_name, salary_new.monthly_salary from employees
join salary_new
on salary_new.salary_new_id = employees.id
inner join it_roles
on it_roles.id = salary_new.salary_new_id
where salary_new.monthly_salary in (1100, 1500, 2000)
